# Entity Relationship Diagram for Tahrir Database

## Diagram

![](https://gitlab.com/fedora/websites-apps/fedora-badges/accolades-api/-/raw/main/docs/dber.png)

## Assets

The related files can be found [here](https://gitlab.com/fedora/websites-apps/fedora-badges/accolades-api/-/blob/main/docs/dber.drawio).
