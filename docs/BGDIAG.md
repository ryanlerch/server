# Entity Relationship Diagram for Badges Database

## Diagram

![](https://gitlab.com/fedora/websites-apps/fedora-badges/accolades-api/-/raw/main/docs/mdrn.png)

## Assets

The related files can be found [here](https://gitlab.com/fedora/websites-apps/fedora-badges/accolades-api/-/blob/main/docs/mdrn.drawio).
